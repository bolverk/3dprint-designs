/*
Copyright 2017 Daniel Brosemer <dan@brosemer.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/
include <ISOThreadUM2.scad>;

module mason_male() {
    $fn=80;
    
    // https://en.wikipedia.org/wiki/Mason_jar
    mouth_od = 70;
    mouth_id = 60;
    
    // https://images-na.ssl-images-amazon.com/images/I/61u7BdyL5WL._SL1001_.jpg
    thread_pitch = 6;
    
    // Measured a Hario
    height = 19;
    thread_depth = 2;
    
    // guessed from from looking at a mason jar
    thread_wraps = 2.9;
    offset_from_top = 3;
    
    // metric_thread() decides thread depth based on angle and pitch
    thread_angle = atan(thread_depth*16/5.3/thread_pitch);
    thread_length=thread_pitch * thread_wraps;
    
    echo("Thread Length: ", thread_length);
    echo("Thread Angle: ", thread_angle);

    difference() {
        union() {
            cylinder(r=(mouth_od-2*thread_depth)/2, h=height);
            // FIXME - Magic 6.8 changes when we change thread_wraps
            translate([0,0,height-thread_length-offset_from_top+6.8])
                thread_out_pitch(mouth_od, thread_length, thread_pitch, thr=80);
        }
        union() {
            cylinder(r=mouth_id/2, h=height);
        }
    }
}