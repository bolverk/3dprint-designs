/*
Copyright 2017 Daniel Brosemer <dan@brosemer.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

gopro2_thickness=15;       // width of the tabs
gopro2_bolt_r = 5 / 2;     // M5 bolt
gopro2_gap_width=3;        // Width of the gap to insert the 3prong mount
gopro2_tab_width=3;        // thickness of tabs to insert into 3prong mount
gopro2_mount_length=gopro2_gap_width+gopro2_tab_width*2;  // Total length of mount
gopro2_connection_thickness = 3;

module GoPro_2Prong()
{
	translate([0,gopro2_connection_thickness,0])
	{
		difference()
		{
			// solid block
			union()
			{
                cube([gopro2_mount_length,10,gopro2_thickness]);
                translate([0,10,gopro2_thickness/2]) rotate([90,0,90]) 
                    cylinder(h=gopro2_mount_length,r=gopro2_thickness/2,$fn=50);
			}

            union() {
                // bolt hole
                translate([-7, 10, gopro2_thickness/2]) rotate([0,90,0]) 
                    cylinder(h=30,r=gopro2_bolt_r,$fn=50);
                // Tabs
                translate([(gopro2_mount_length/2)-gopro2_gap_width/2,0,0]) 
                    cube([gopro2_gap_width,30,40]);
            }
		}
	}

	// connection block
	cube([gopro2_mount_length,gopro2_connection_thickness,gopro2_thickness]);
}


