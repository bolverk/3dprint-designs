/*
Copyright 2017 Daniel Brosemer <dan@brosemer.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

gopro3_thickness=15;       // width of the tabs
gopro3_bolt_r = 5 / 2;     // M5 bolt
gopro3_nut_r = 4.8;        // Head size for M5 nut - validate
gopro3_middle_width=2.7;   // Width of the middle tab
gopro3_gap_width=3.5;      // Width of the gaps to insert the 2prong mount
gopro3_mount_length=15.1;  // Total length of mount not including the captive nut holder
gopro3_nut_height=4;       // Height of the captive nut holder

module GoPro_3Prong()
{
	translate([0,3,0])
	{
		difference()
		{
			// solid block
			union()
			{
                cube([gopro3_mount_length,10,gopro3_thickness]);
                translate([0,10,gopro3_thickness/2]) rotate([90,0,90]) 
                    cylinder(h=gopro3_mount_length+gopro3_nut_height,r=gopro3_thickness/2,$fn=50);
			}

            union() {
                // bolt hole
                translate([-7, 10, gopro3_thickness/2]) rotate([0,90,0]) 
                    cylinder(h=30,r=gopro3_bolt_r,$fn=50);
                // Captive nut
                translate([gopro3_mount_length, 10, gopro3_thickness/2]) rotate([0,90,0]) 
                    cylinder(gopro3_nut_height*1.1, r=gopro3_nut_r, $fn=6);
                // Tabs
                for(offset=[(gopro3_mount_length-gopro3_middle_width)/2-gopro3_gap_width,
                            (gopro3_mount_length-gopro3_middle_width)/2+gopro3_middle_width]) {
                    translate([offset,0,0]) cube([3.5,30,40]);
                }
            }
		}
	}

	// connection block
	cube([gopro3_mount_length,3,gopro3_thickness]);
}



