/*
Copyright 2017 Daniel Brosemer <dan@brosemer.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

module bontrager_light_mount(base_plate_thickness = 2) {
    
    width = 12.5;
    height = 16.5;
    ridge_center = 8;
    latch_distance = 16;
    
    top_plate_thickness = 2;
    top_plane = 2.5 + base_plate_thickness + top_plate_thickness;
    
    difference() {
        union() {
            // top plate
            translate([0,0,base_plate_thickness+2.5]) 
                cube([width,height,top_plate_thickness]);
            
            //middle section
            translate([(width-10)/2,(height-10)/2,base_plate_thickness])
                cube([10,10,2.5]);
            
            // base plate
            cube([width,height,base_plate_thickness]);
            
            // retention ridges
            translate([(width-ridge_center)/2,height/2+(height-2)/2,top_plane]) 
                rotate([90,0,0]) cylinder(h=height-2, r=.5, $fn=30);
            translate([(width+ridge_center)/2,height/2+(height-2)/2,top_plane]) 
                rotate([90,0,0]) cylinder(h=height-2, r=.5, $fn=30);
            
            // Latch
            translate([latch_distance-1,(height-5)/2,top_plane])
                cube([1,5,1]);
            translate([latch_distance - 1.5,(height-5)/2,top_plane + 0.1])
                rotate([-90,60,0]) cylinder(r=1,h=5,$fn=3);
            translate([width,(height-7)/2,2.5 + base_plate_thickness])
                cube([latch_distance-width+1,7,top_plate_thickness]);
                
            // Lever
            translate([latch_distance+1,(height-10)/2,2.5 + base_plate_thickness])
                cube([4,10,top_plate_thickness]);
            
            translate([latch_distance+4,(height-10)/2,2.5 + base_plate_thickness])
                rotate([0,10,0]) {
                    cube([6,10,top_plate_thickness]);
                    translate([6,5,0])
                        cylinder(r=5,h=top_plate_thickness, $fn=30);
                }
        }
        union() {
        }
    }
}