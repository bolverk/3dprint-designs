$fn=64;

mount_height = -32.5;
mount_x = 39;
mount_thickness = 5;

difference() {
union() {
    translate([0,40,0])
        import("lib/stl/Anet ET4-5 E3DV6-Revo Six-BMO Gantry Adapter.stl");
    
    // Plug holes
    translate([32,-37.2,0])
        cube([7,25.87,5]);
    translate([0,-37.2,0])
        cube([7,25.87,5]);
    
    translate([mount_x,-37.2,0])
        cube([7.5,25.87,mount_thickness]);
}
union() {
    translate([mount_x+7.5/2, mount_height, -.1])
        cylinder(r=3.5/2,h=mount_thickness+.2);
    translate([mount_x+7.5/2, mount_height, -.1])
        cylinder(r=6/2,h=mount_thickness-3.5+.1);
    
    translate([mount_x+7.5/2, mount_height +15, -.1])
        cylinder(r=3.5/2,h=mount_thickness+.2);
    translate([mount_x+7.5/2, mount_height +15, -.1])
        cylinder(r=6/2,h=mount_thickness-3.5+.1);
    
    // Remove nubs at the bottom
    translate([-1,-42,-.1])
        cube([50,5,5.2]);
}
}