include <lib/mason.scad>;
include <lib/funnel.scad>;

// Width of stand is 87
// Height from bottom of threads to bottom of spout should be 55

// CONSTANTS
mason_od = 70;
mason_thread_depth = 2;
mason_id = 60;

// board_thickness=.75 * 25.4;  // 1X dimensioned lumber
board_thickness=0;
sheet_thickness = 10;  // Thickness of mounting sheet
sheet_x = 87;         // Mounting sheet size
sheet_y = 87;
// https://www.trfastenings.com/Products/knowledgebase/Tables-Standards-Terminology/Tapping-Sizes-and-Clearance-Holes
screw_dia=4.5;  // Clearance hole for M4 bolt

spout_od=48;       // A little smaller than portafilter basket
spout_h=5;         // Height of straight part of spout
funnel_h=55-sheet_thickness-board_thickness;       // Height of offset part of funnel
funnel_offset=20;  // How far forward to offset the spout

// Build funnel
funnel(mason_od, spout_od, funnel_h, sheet_thickness, spout_h, funnel_offset,
        (mason_od-mason_id)/2);

// Build mounting sheet
diagonal = sqrt(sheet_x*sheet_x+sheet_y*sheet_y);
difference() {
    translate([0,0,sheet_thickness/2]) 
        cube([sheet_x,sheet_y,sheet_thickness], center=true);
    union() {
        cylinder(r=mason_id/2,h=sheet_thickness,$fn=50);
        // Screw holes
        /*
        for(r=[0:4]) {
            rotate([0,0,r*360/4+45]) 
                translate([35+(diagonal/2-40)/2, 0, 0]) 
                    cylinder(r=screw_dia/2,h=sheet_thickness, $fn=35);
        }
        */
    }
}

// Build mason jar thread
translate([0,0,-19-board_thickness])
    mason_male();
translate([0,0,-board_thickness-1])
    difference() {
        cylinder(r=(mason_od-2*mason_thread_depth)/2,h=board_thickness+1,$fn=80);
        cylinder(r=mason_id/2,h=board_thickness+1,$fn=80);
    }


