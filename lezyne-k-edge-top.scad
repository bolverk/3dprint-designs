$fn=50;

difference() {
    translate([0,0,-1])
        import("lib/stl/HAUT-LEZYNE_K-EDGE.stl", 5);

    union() {
        translate([-20,-20,-5])
            cube([50,50,5]);
    }
}

for(r=[45])
    rotate([0,0,r]) {
        translate([14.5,0,-3])
            cylinder(8, 1.4, 1.4);
        translate([-14.5,0,-3])
            cylinder(8, 1.4, 1.4);
    }