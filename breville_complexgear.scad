include <lib/gears.scad>

/*  Spur gear
    modul = Height of the Tooth Tip beyond the Pitch Circle
    tooth_number = Number of Gear Teeth
    width = tooth_width
    bore = Diameter of the Center Hole
    pressure_angle = Pressure Angle, Standard = 20° according to DIN 867. Should not exceed 45°.
    helix_angle = Helix Angle to the Axis of Rotation; 0° = Spur Teeth
    optimized = Create holes for Material-/Weight-Saving or Surface Enhancements where Geometry allows */

big_teeth=56;
big_tooth_depth=.8;
big_height=11;
big_angle=15;

little_teeth=12;
little_tooth_depth=1.1;
little_height=15;

base_cut_depth=4;
base_cut_dia=20.7;

shaft_length=32.25;
shaft_outer=11.8;
shaft_above=1.5;
center_bore=8;
lower_bush_dia=12.7;

difference() {
    union() {
        // Fake this to make the teeth taller
        translate([0,0,big_height])
            difference() {
                spur_gear(little_tooth_depth+.13,little_teeth,little_height,center_bore, pressure_angle=20, helix_angle=0, optimized=false);
                cylinder(h=little_height,r=shaft_outer/2,center=false,$fn=100);
            }
        translate([0,0,big_height])
            spur_gear(little_tooth_depth,little_teeth,little_height,center_bore, pressure_angle=0, helix_angle=0, optimized=false);

        spur_gear(big_tooth_depth,big_teeth,big_height,center_bore, pressure_angle=20, helix_angle=big_angle, optimized=false);
    }
    cylinder(h=base_cut_depth,r=base_cut_dia/2,center=false,$fn=100);

};
translate([0,0,shaft_above-shaft_length+big_height+little_height])
    difference() {
        union() {
            cylinder(h=shaft_length,r=shaft_outer/2,center=false,$fn=100);
            cylinder(
                h=shaft_length-big_height-little_height+base_cut_depth,
                r=lower_bush_dia/2,center=false,$fn=100);
        }
        cylinder(h=shaft_length,r=center_bore/2,center=false,$fn=100);
    }