// motor to funnel is 35

orig_thickness=38;
desired_thickness=35;

scale(v=[1,desired_thickness/orig_thickness,1])
    import("lib/stl/Coffee_Grinder_Hopper_and_Motor_Mount.stl", convexity = 4);