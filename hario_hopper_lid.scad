lid_depth=3;
lid_width_outer=45;
lid_width_inner=40;
lid_length=40;
flange_depth=2;

ridge_outer=3;

LidPoints = [
  [          0,                                                   0,          0 ],  //0
  [ lid_length,                 (lid_width_outer-lid_width_inner)/2,          0 ],  //1
  [ lid_length, lid_width_outer-(lid_width_outer-lid_width_inner)/2,          0 ],  //2
  [          0,                                     lid_width_outer,          0 ],  //3
  [          0,                                                   0,  lid_depth ],  //4
  [ lid_length,                 (lid_width_outer-lid_width_inner)/2,  lid_depth ],  //5
  [ lid_length, lid_width_outer-(lid_width_outer-lid_width_inner)/2,  lid_depth ],  //6
  [          0,                                     lid_width_outer,  lid_depth ]]; //7
  
LidFaces = [
  [0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [7,6,5,4],  // top
  [5,6,2,1],  // right
  [6,7,3,2],  // back
  [7,4,0,3]]; // left

difference() {
    union() {
        polyhedron(LidPoints, LidFaces);
        translate([ridge_outer, ridge_outer,-1*flange_depth])
            scale(v=[(lid_length-ridge_outer*2)/lid_length,
                    (lid_width_outer-ridge_outer*2)/lid_width_outer,
                    flange_depth/lid_depth])
                polyhedron(LidPoints, LidFaces);
    }
    union() {
        translate([lid_length/2, lid_width_outer/2, -10])
            cylinder(r=3, h=20, $fn=50);
    }
}
