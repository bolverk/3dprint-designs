$fn=32;

door_hole=17.5;
// inset = 7.1/2;

flange_depth = 3;
// flange_depth=0;
flange_dia=door_hole+10;
id = 6.32;
// id = 9.2;

standoff_od = 12;
standoff_id = id;
standoff_len = 2;

inset = 7 - standoff_len;

difference() {
    union() {
        cylinder(h=standoff_len, d=standoff_od, center=false);
        translate([0,0,standoff_len])
            cylinder(h=inset, d=door_hole, center=false);
        translate([0,0,standoff_len + inset])
            cylinder(h=flange_depth, d=flange_dia, center=false);
    };
    union() {
        cylinder(h=standoff_len + inset+flange_depth, d=id, center=false);
    };
}