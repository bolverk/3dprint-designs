include <lib/triangles.scad>

sstay_a=90-38;  // Measured from Farley

light_l = 86;
sstay_l = light_l;

light_w = 24;

zip_w = 6;  // zip tie is 5mm
zip_h = 2;  // zip tie is 1.5mm

strap_size = 20;  // measured at 25
ear_offset = 58;  // distance from bottom of light to ears

difference() {
    union() {
        Triangle(a=light_l, b=sstay_l, angle=sstay_a, height=light_w);
        
        // Ear for light
        translate([ear_offset,0,light_w/2])
            rotate(a=180, v=[1,0,0])
                ears(r=strap_size/2, flange_r=strap_size/2+4, flange_w=zip_w, 
                    ear_w=light_w+zip_w*2);
        
/*        // Ear for seatstay
        rotate(a=sstay_a, v=[0,0,1])
            translate([ear_offset,0,light_w/2])
                ears(r=strap_size/2, flange_r=strap_size/2+4, flange_w=5, 
                    ear_w=light_w+10); */

        // Round it off for the light mount
        rotate(a=90, v=[0,1,0])
            translate([-light_w/2,0,0])
                semicylinder(r= light_w/2, h= light_l);        
    };
    union() {
        // Hollow it out for the seatstay
        rotate(a=sstay_a, v=[0,0,1])
        rotate(a=90, v=[0,1,0])
            translate([-light_w/2,0,-10])
                cylinder(r= light_w/2, h= light_l+10);  
        
        // Zip tie holes for seatstay
        rotate(a=sstay_a, v=[0,0,1])
            translate([sstay_l/3,-light_w/2-zip_w,-1])
                cube([zip_w,zip_h,light_w+2]);
        rotate(a=sstay_a, v=[0,0,1])
            translate([sstay_l/3*2,-light_w/2-zip_w,-1])
                cube([zip_w,zip_h,light_w+2]);
    };
};

module ears(r, flange_r, flange_w, ear_w) {
    difference() {
        union() {
            translate([0,0,-ear_w/2])
                cylinder(r=r, h=ear_w);
            translate([0,0,-ear_w/2-flange_w])
                cylinder(r=flange_r, h=flange_w);
            translate([0,0,ear_w/2])
                cylinder(r=flange_r, h=flange_w);
        };
        union() {
            translate([-flange_r,0,-flange_w-ear_w/2])
                cube([flange_r*2,flange_r*2,flange_w*2+ear_w]);
        };
    };
};

module semicylinder(r, h) {
    difference() {
        cylinder(r=r, h=h);
        translate([-r, 0, 0])
            cube([r*2, r*2, h]);
    }
}