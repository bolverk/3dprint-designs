$fn = 32;
difference() {
    union() {
        difference() {
            import("lib/stl/transmitr_handlebar.stl", convexity=3);
            translate([-43.5,-5,0]) {
                cube([40,50,40]);
            };
        };
        translate([-3.5,18,5]) {
            rotate([0,-90,0])
                import("lib/stl/garmin_male.stl", convexity=3);
        };
    };
    union() {
        translate([0,3,5]) {
            rotate([0,-90,0])
                cylinder(h = 30, r = 1.5, center = true);
        }
        translate([0,33,5]) {
            rotate([0,-90,0])
                cylinder(h = 30, r = 1.5, center = true);
        }
        translate([-5,3,5]) {
            rotate([0,-90,0])
                cylinder(h = 6, r = 2.5, center = true);
        }
        translate([-5,33,5]) {
            rotate([0,-90,0])
                cylinder(h = 6, r = 2.5, center = true);
        }
    };
};
