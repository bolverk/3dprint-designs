/*
Copyright 2017 Daniel Brosemer <dan@brosemer.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/
include <lib/bontrager_light.scad>

rack_mount_depth = 5;
rack_mount_width = 17;
rack_hole_centers = 20;
// https://www.trfastenings.com/Products/knowledgebase/Tables-Standards-Terminology/Tapping-Sizes-and-Clearance-Holes
rack_bolt_dia = 5.5;  // M5
bolt_head_dia = 8;
$fn=60;

/* bontrager_light.scad
    width = 12.5;
    height = 16.5;  */

difference() {
    union() {
        cube([rack_mount_width,rack_hole_centers,rack_mount_depth]);
        translate([rack_mount_width/2, 0, 0])
            cylinder(r=rack_mount_width/2, h=rack_mount_depth);
        translate([rack_mount_width/2, rack_hole_centers, 0])
            cylinder(r=rack_mount_width/2, h=rack_mount_depth);

        translate([(rack_mount_width-12.5)/2,rack_hole_centers-16.5/2,rack_mount_depth])
            bontrager_light_mount(0);
    }
    union() {
        // Bolt holes
        translate([rack_mount_width/2, 0, 0])
            cylinder(r=rack_bolt_dia/2, h=20);
        translate([rack_mount_width/2, rack_hole_centers, 0])
            cylinder(r=rack_bolt_dia/2, h=20);
        
        // Bolt head
        translate([rack_mount_width/2, rack_hole_centers, rack_mount_depth])
            cylinder(r=bolt_head_dia/2, h=20);
    }
}