$fn=32;

cable_angle=60;
cable_bore=4.5;
sheath=7;

cable_z=0;
cable_x=-15;

screw_y = 2.5;
screw_x = -25;

difference() {
    union() {
        mirror([1,0,0])
            rotate([0,0,0]) // STL isn't perfectly rotated
                    import(file = "/home/odin/farley_lower_blended_fixed.stl");
        
        translate([cable_x,screw_y,cable_z])
            rotate([0,cable_angle,0])
                cylinder(d=sheath,h=17);
    }
    union() {
        // Bore a hole for the cable
        translate([cable_x,screw_y,cable_z])
            rotate([0,cable_angle,0])
                translate([0,0,-14])
                    cylinder(d=cable_bore,h=40);
        
        // Bore bigger so we can fish
        /*translate([-5,6,cable_z-.5])
            rotate([0,cable_angle,0])
                translate([1,0,-11])
                    cylinder(d=sheath,h=11); */
        
        // Screw hole    
        translate([screw_x,screw_y,2])
            cylinder(d=3.15,h=20);
        
        // Counter sink
        translate([screw_x,screw_y,3])
            mirror([0,0,1])
                linear_extrude(height = 1, scale = 2, convexity = 10)
                    circle(d=3.15);
        
        // Bore to access screw
        translate([screw_x,screw_y,-1])
            cylinder(d=6.3,h=3);
            
        // Flat around underside of screw hole
        rotate([5,0,0])
            translate([screw_x,screw_y,3.94])
                cylinder(d=9.25,h=20);
            
        // Chop off junk
        translate([-40,-40,-10])
            cube([80,80,10]);
    }
}