$fn=128;
radius=200;      // radius of the shelf
thickness=10;    // thickness of the shelf
previs=thickness+5; // this is the front side of the shelf (let's make it a bit bigger) - default value should be OK
support_l=40;   // length of support for screw
screw_d=4.2;      // screw diameter
support_thickness=7.5;        // thickness of the "leg" (default value is usually OK)
support_width=screw_d+10; //  width of the "leg" (defined as screw hole + 10mm)
corner_hole=5;             // radius of corner hole (for cables etc.)

corner_angle=92.5;

countersink_scale=2;
countersink_h=2.5;

// main code

difference() {
    union() {
     shelf(radius,thickness,previs,corner_angle);
    
     translate([radius/2+support_width/2,0,thickness]) rotate([90,0,180]) 
      support(support_l,screw_d,support_width,support_thickness,countersink_scale,countersink_h,countersink_d);

    rotate([0,0,corner_angle]) 
      translate([radius/2-support_width/2,0,thickness])
        rotate([90,0,0])
          support(support_l,screw_d,support_width,support_thickness,countersink_scale,countersink_h,countersink_d);
    }
    cylinder(r=corner_hole,h=thickness+0.1); // corner hole
}
    

module shelf(radius,thickness,previs) {
 difference() {
  cylinder(r=radius,h=previs);
  translate([0,0,thickness]) cylinder(r=radius-thickness,h=thickness+100);
  translate([-0.1-radius,-radius,-0.1])
    cube([radius*2+0.2,radius+0.1,thickness+previs+0.2]);
  rotate([0,0,corner_angle-90])
    translate([-radius-0.1,-radius-0.1,-0.1]) 
      cube([radius+0.1,radius*2+0.1,thickness+previs+0.2]);
 }
    
}

module support(support_l,screw_d,support_width,support_thickness,countersink_scale,countersink_h) {
    difference() {
      translate([0,0,support_thickness]) 
        cube([support_width,support_l*0.70,support_l*0.70]);
      union() {
        translate([-0.1,support_l*0.70,support_thickness+support_l*0.70])
          rotate([0,90,0])  
            cylinder(r=support_l*0.70,h=support_width+0.2);
        translate([support_width/2,support_l*0.75,(support_thickness-countersink_h)+countersink_h]) cylinder(d=screw_d*countersink_scale,h=support_thickness+0.2);
      }
    }
    
    difference() {
     union() {
      cube([support_width,support_l-support_width/2,support_thickness]);
      translate([support_width/2,support_l-support_width/2,0]) cylinder(d=support_width,h=support_thickness);
     }
     union() {
       translate([support_width/2,support_l*0.75,-0.1]) cylinder(d=screw_d,h=support_thickness+0.2);
       translate([support_width/2,support_l*0.75,(support_thickness-countersink_h)])
         linear_extrude(height = countersink_h, scale = countersink_scale, convexity = 10)
           circle(d=screw_d);
       translate([support_width/2,support_l*0.75,(support_thickness-countersink_h)+countersink_h]) cylinder(d=screw_d*countersink_scale,h=support_thickness+0.2);
     }
    }
}