$fn=32;

od = 10;
id = 6.4;
l = 13;

difference() {
    union() {
        cylinder(h=l, d=od, center=false);
    };
    union() {
        cylinder(h=l, d=id, center=false);
    };
}